package moxylib

import (
	"log"
	"sort"
	"sync"
	"time"

	"golang.org/x/time/rate"
)

type Ban struct {
	IP     string `mold:"trim" validate:"required,ipv4"`
	Reason string `mold:"trim" validate:"required"`
}

type bLimiter struct {
	*rate.Limiter
	Blocked    bool
	Requests   int
	Violations int
	Current    time.Time
}

type LimiterStat struct {
	IP         string
	Requests   int
	Violations int
	Current    int64
}

var (
	limiters        sync.Map
	LimiterBanCount int
	LimiterBurst    int
	LimiterRate     int
)

func limitersLoad() error {
	limiters.Clear()
	bans, err := KVSetting.GETALL()
	if err != nil {
		return err
	}
	for ip := range bans {
		LimitersStore(ip, true)
	}
	log.Println("Limiters Loaded")
	return nil
}

func NewLimiter(ban bool) *bLimiter {
	return &bLimiter{rate.NewLimiter(rate.Limit(LimiterRate), LimiterBurst), ban, 0, 0, time.Now().Truncate(time.Hour * 24)}
}

func LimitersDelete(ip string) {
	limiters.Delete(ip)
}

func LimitersLoadStore(ip string) *bLimiter {
	l, _ := limiters.LoadOrStore(ip, NewLimiter(false))
	return l.(*bLimiter)
}

func LimitersStore(ip string, ban bool) {
	limiters.Store(ip, NewLimiter(ban))
}

func LimitersStats(ignore string) []LimiterStat {
	var stats []LimiterStat
	limiters.Range(func(key, value any) bool {
		ip := key.(string)
		if limiter, ok := value.(*bLimiter); ok && !limiter.Blocked && ip != ignore {
			stats = append(stats, LimiterStat{
				IP:         ip,
				Requests:   limiter.Requests,
				Violations: limiter.Violations,
				Current:    limiter.Current.Unix(),
			})
		}
		return true
	})
	sort.Slice(stats, func(i, j int) bool {
		return stats[i].Requests > stats[j].Requests
	})
	return stats
}

// KEEP THESE EXAMPLES FOR REFERENCE

// type wrappedWriter struct {
// 	http.ResponseWriter
// 	statusCode int
// }

// func (w *wrappedWriter) WriteHeader(status int) {
// 	w.ResponseWriter.WriteHeader(status)
// 	w.statusCode = status
// }

// // Log logs the request method, status code, and path.
// func Log(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		start := time.Now()
// 		wrapped := &wrappedWriter{w, http.StatusOK}
// 		next.ServeHTTP(wrapped, r)
// 		log.Println(r.RemoteAddr, r.Method, wrapped.statusCode, r.URL.Path, time.Since(start))
// 	})
// }

// func RateLimiter(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		ip, _, err := net.SplitHostPort(r.RemoteAddr)
// 		if err != nil {
// 			http.Error(w, "Invalid IP address", 555)
// 			return
// 		}
// 		limiter := LimitersLoadStore(ip)
// 		if limiter.Blocked {
// 			http.Error(w, "IP blocked for abuse", http.StatusForbidden)
// 			return
// 		}
// 		limiter.Requests++
// 		if !limiter.Allow() {
// 			http.Error(w, "Too Many Requests", http.StatusTooManyRequests)
// 			return
// 		}
// 		wrapped := &wrappedWriter{w, http.StatusOK}
// 		next.ServeHTTP(wrapped, r)
// 		if 400 > wrapped.statusCode || wrapped.statusCode >= 500 {
// 			return
// 		}
// 		timeNow := time.Now().Truncate(time.Hour * 24)
// 		if limiter.Current != timeNow {
// 			limiter.Current = timeNow
// 			limiter.Violations = 0
// 		}
// 		var reason string
// 		switch wrapped.statusCode {
// 		case http.StatusUnauthorized:
// 			limiter.Violations += 5
// 			reason = "401 Unauthorized: Automated Ban"
// 		case http.StatusTeapot:
// 			limiter.Violations += 10
// 			reason = "418 Teapot: Automated Ban"
// 		default:
// 			limiter.Violations++
// 			reason = strconv.Itoa(wrapped.statusCode) + " Bad Request: Automated Ban"
// 		}
// 		if limiter.Violations > LimiterBanCount {
// 			limiter.Blocked = true
// 			if err := KVBan.SET(map[string]string{ip: reason}); err != nil {
// 				w.Write([]byte(err.Error()))
// 				return
// 			}
// 			w.Write([]byte("IP blocked for abuse\n"))
// 		}
// 	})
// }
