package moxylib

import (
	"fmt"
	"reflect"
	"slices"
	"sync"
	"testing"
)

// TestGoRangeWait tests the GoRangeWait function.
func TestGoRangeWait(t *testing.T) {
	list := []int{1, 2, 3, 4, 5}
	expectedErrors := []string{"error1", "error3", "error5"}
	var mu sync.Mutex
	var actualErrors []error

	gofunc := func(i int, v int) error {
		if v%2 == 1 {
			err := fmt.Errorf("error%d", v)
			mu.Lock()
			actualErrors = append(actualErrors, err)
			mu.Unlock()
			return err
		}
		return nil
	}
	errs := GoRangeWait(list, gofunc)
	if errs == nil {
		t.Errorf("expected errors")
	}
	var string_errs []string
	for _, err := range errs {
		if err != nil {
			string_errs = append(string_errs, err.Error())
		}
	}
	slices.Sort(string_errs)
	if !slices.Equal(string_errs, expectedErrors) {
		t.Errorf("expected %v, got %v", expectedErrors, string_errs)
	}
}

// TestStructToMap tests the StructToMap function.
func TestStructToMap(t *testing.T) {
	type FormData struct {
		Name  string
		Email string
		Num   int
	}
	form := &FormData{Name: "John Doe", Email: "test@gmail.com", Num: 10}
	m := StructMap(form)
	expected := map[string]string{"John Doe": "test@gmail.com"}
	if !reflect.DeepEqual(m, expected) {
		t.Errorf("expected %v, got %v", expected, m)
	}
}

// TestTransform tests the Transform function.
func TestTransform(t *testing.T) {
	type FormData struct {
		Name  string `validate:"required"`
		Email string `validate:"required,email"`
	}

	form := &FormData{Name: "John Doe", Email: "john.doe@example.com"}
	if err := Transform(form); err != nil {
		t.Fatalf("an error '%s' was not expected during Transform", err)
	}

	expected := &FormData{Name: "John Doe", Email: "john.doe@example.com"}
	if !reflect.DeepEqual(form, expected) {
		t.Errorf("expected %v, got %v", expected, form)
	}
}
