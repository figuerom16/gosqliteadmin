package moxylib

import (
	"fmt"
	"os"
	"path/filepath"
)

type FileInfo struct {
	Name    string
	Path    string
	Size    int64
	Created int64
}

// Backup Sqlite3 database to a file.
func CreateBackup(path, name string) (*FileInfo, error) {
	if _, err := DB.Exec(fmt.Sprintf("VACUUM INTO '%s/%s';", path, name)); err != nil {
		return nil, err
	}
	backup, err := GetFile(path, name)
	if err != nil {
		return nil, err
	}
	return backup, nil
}

// Delete a file from the filesystem.
func DeleteFile(path, name string) error {
	return os.Remove(filepath.Join(path, name))
}

// Get a file from the filesystem.
func GetFile(path, name string) (*FileInfo, error) {
	fileInfo, err := os.Stat(filepath.Join(path, name))
	if err != nil {
		return nil, err
	}
	return &FileInfo{Name: name, Path: path, Size: fileInfo.Size(), Created: fileInfo.ModTime().Unix()}, nil
}

// List files in a directory.
func ListFiles(path string) ([]*FileInfo, error) {
	entries, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}
	var files []*FileInfo
	for _, e := range entries {
		eInfo, err := e.Info()
		if err != nil {
			return nil, err
		}
		if e.Name() != ".gitkeep" {
			files = append(files, &FileInfo{Name: e.Name(), Path: path, Size: eInfo.Size(), Created: eInfo.ModTime().Unix()})
		}
	}
	return files, nil
}
