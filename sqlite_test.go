package moxylib

import (
	"database/sql"
	"os"
	"path/filepath"
	"testing"
)

type Testuser struct {
	Id   int
	Name string
}

func TestMain(m *testing.M) {
	if err := setupTesDB(); err != nil {
		panic(err)
	}
	defer os.RemoveAll("sqlite")
	m.Run()
}

func setupTesDB() error {
	dbPath := "sqlite/sqlite"
	if err := os.MkdirAll(filepath.Dir(dbPath), 0770); err != nil {
		return err
	}
	newDB, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		return err
	}
	_, err = newDB.Exec("CREATE TABLE IF NOT EXISTS test_kv (K TEXT PRIMARY KEY, V TEXT NOT NULL);")
	if err != nil {
		return err
	}
	DB = &MDB{newDB}
	return nil
}

func Test(t *testing.T) {
	migrationContent := `
--moxylib
CREATE TABLE test (id INTEGER PRIMARY KEY);
CREATE TABLE user (Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT);
--moxylib
ALTER TABLE test ADD COLUMN name TEXT;
INSERT INTO user (name) VALUES ('test');
INSERT INTO user (name) VALUES ('test2');
`
	migrationFile := filepath.Join(os.TempDir(), "migration.sql")
	if err := os.WriteFile(migrationFile, []byte(migrationContent), 0644); err != nil {
		t.Fatalf("Failed to write migration file: %v", err)
	}
	defer os.Remove(migrationFile)
	if err := Migrate(migrationFile); err != nil {
		t.Fatalf("Migrate() error = %v", err)
	}
	version, err := GetUserVersion()
	if err != nil {
		t.Fatalf("GetUserVersion() error = %v", err)
	}
	if version != 2 {
		t.Errorf("GetUserVersion() = %v, want %v", version, 2)
	}

	// Verify the table and column exist
	var count int
	if err := DB.QueryRow("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='test'").Scan(&count); err != nil {
		t.Fatalf("QueryRow() error = %v", err)
	}
	if count != 1 {
		t.Errorf("Table 'test' does not exist")
	}

	err = DB.QueryRow("SELECT COUNT(*) FROM pragma_table_info('test') WHERE name='name'").Scan(&count)
	if err != nil {
		t.Fatalf("QueryRow() error = %v", err)
	}
	if count != 1 {
		t.Errorf("Column 'name' does not exist in table 'test'")
	}

	// Test RowsScan
	rows, err := DB.Query("SELECT * FROM user;")
	if err != nil {
		t.Fatalf("Query() error = %v", err)
	}
	var users []*Testuser
	if err := RowsScan(rows, &users); err != nil {
		t.Fatalf("RowsScan() error = %v", err)
	}
	if len(users) != 2 {
		t.Errorf("RowsScan() = %v, want %v", len(users), 2)
	}
	if users[0].Name != "test" {
		t.Errorf("RowsScan() = %v, want %v", users[0].Name, "test")
	}
	if users[1].Name != "test2" {
		t.Errorf("RowsScan() = %v, want %v", users[1].Name, "test2")
	}
}
