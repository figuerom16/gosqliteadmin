# MoxyLib

This is a small *sql.DB SQLite wrapper to provide extra functionality. The focus of the lib is to:
- Utilizes Go github.com/ncruces/go-sqlite3 as the default, but can accept other drivers.
- Take SQLite commands as text and get an output that is renderable to HTML.
- Transaction wrapper and Up only automatic migration system using PRAGMA user_version.
- Store and retrieve settings/config in SQLite using a Key/Value table.
- Key/Value storage functions for Value, Set, List, Hash.

### Key/Value methods
**Note: all values are saved as strings and use '\n' character as the separator. This makes any method other than the simple KVV not suitable for storage of data containing '\n' characters unless the data is Base64 encoded first.**
- KVV: Basic Key Value methods. Everything in the Value column is a string and will be retrieved and set as such.
- KVH: Hash Values with fields. Field and Value is separated by '\' and only splits on first occurence of separator.
- KVL: List Values allows for repeats and has push pop functionality.
- KVS: Sets are lists except they don't allow for duplicates and is sorted.


## Special Thanks to the Developers of:
go-sqlite3 - https://github.com/ncruces/go-sqlite3  
form,mold,validator - https://github.com/go-playground  
argon2id - https://github.com/alexedwards/argon2id  
zstd - https://github.com/klauspost/compress  

And of course the Go Standard Lib.

![sqlite](https://gitlab.com/figuerom16/moxylib/-/raw/main/_example/screenshots/sqlite.png)
![sqlite](https://gitlab.com/figuerom16/moxylib/-/raw/main/_example/screenshots/settings.png)