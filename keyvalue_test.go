package moxylib

import (
	"database/sql"
	"strings"
	"testing"
)

var kv = KVTable{
	Table: "test_kv",
	CK:    "K",
	CV:    "V",
}

type kvStruct struct {
	K string
	V string
}

func TestKVH(t *testing.T) {
	key := "test_key"
	initialData := map[string]string{
		"field1": "value1",
		"field2": "value2",
	}

	// Test HSET
	if err := kv.HSET(key, initialData); err != nil && err != sql.ErrNoRows {
		t.Fatalf("HSET() error = %v", err)
	}

	// Test HGET
	retrievedData, err := kv.HGET(key)
	if err != nil {
		t.Fatalf("HGET() error = %v", err)
	}
	if len(retrievedData) != len(initialData) {
		t.Fatalf("HGET() = %v, want %v", retrievedData, initialData)
	}
	for field, value := range initialData {
		if retrievedData[field] != value {
			t.Errorf("HGET() = %v, want %v", retrievedData[field], value)
		}
	}

	// Test KVHDel
	if err := kv.HDEL(key, "field1"); err != nil {
		t.Fatalf("HDEL() error = %v", err)
	}
	retrievedData, err = kv.HGET(key)
	if err != nil {
		t.Fatalf("HGET() error = %v", err)
	}
	if _, exists := retrievedData["field1"]; exists {
		t.Errorf("HDEL() did not delete field1")
	}
	if retrievedData["field2"] != "value2" {
		t.Errorf("HGET() = %v, want %v", retrievedData["field2"], "value2")
	}
}

func TestKVL(t *testing.T) {
	key := "test_key1"
	initialValues := []string{"value1", "value2", "value3"}

	// Set initial values
	if err := kv.SET(map[string]string{key: strings.Join(initialValues, "\n")}); err != nil {
		t.Fatalf("SET() error = %v", err)
	}

	// Test LGET
	values, err := kv.LGET(key)
	if err != nil {
		t.Fatalf("LGET() error = %v", err)
	}
	if len(values) != len(initialValues) {
		t.Fatalf("LGET() = %v, want %v", values, initialValues)
	}
	for i, v := range initialValues {
		if values[i] != v {
			t.Errorf("LGET() = %v, want %v", values[i], v)
		}
	}

	// Test LEXISTS
	exists, err := kv.LEXISTS(key, "value2")
	if err != nil {
		t.Fatalf("LEXISTS() error = %v", err)
	}
	if !exists {
		t.Errorf("LEXISTS() = %v, want true", exists)
	}
	exists, err = kv.LEXISTS(key, "value4")
	if err != nil {
		t.Fatalf("LEXISTS() error = %v", err)
	}
	if exists {
		t.Errorf("LEXISTS() = %v, want false", exists)
	}

	// Test LPUSH
	if err := kv.LPUSH(key, "value4", true); err != nil {
		t.Fatalf("LPUSH() error = %v", err)
	}
	values, err = kv.LGET(key)
	if err != nil {
		t.Fatalf("LGET() error = %v", err)
	}
	expectedValues := append(initialValues, "value4")
	if len(values) != len(expectedValues) {
		t.Fatalf("LGET() = %v, want %v", values, expectedValues)
	}
	for i, v := range expectedValues {
		if values[i] != v {
			t.Errorf("LGET() = %v, want %v", values[i], v)
		}
	}

	if err := kv.LPUSH(key, "value0", false); err != nil {
		t.Fatalf("LPUSH() error = %v", err)
	}
	values, err = kv.LGET(key)
	if err != nil {
		t.Fatalf("LGET() error = %v", err)
	}
	expectedValues = append([]string{"value0"}, expectedValues...)
	if len(values) != len(expectedValues) {
		t.Fatalf("LGET() = %v, want %v", values, expectedValues)
	}
	for i, v := range expectedValues {
		if values[i] != v {
			t.Errorf("LGET() = %v, want %v", values[i], v)
		}
	}

	// Test LPOP
	poppedValue, err := kv.LPOP(key, true)
	if err != nil {
		t.Fatalf("LPOP() error = %v", err)
	}
	if poppedValue != "value4" {
		t.Errorf("LPOP() = %v, want %v", poppedValue, "value4")
	}
	values, err = kv.LGET(key)
	if err != nil {
		t.Fatalf("LGET() error = %v", err)
	}
	expectedValues = expectedValues[:len(expectedValues)-1]
	if len(values) != len(expectedValues) {
		t.Fatalf("LGET() = %v, want %v", values, expectedValues)
	}
	for i, v := range expectedValues {
		if values[i] != v {
			t.Errorf("LGET() = %v, want %v", values[i], v)
		}
	}

	poppedValue, err = kv.LPOP(key, false)
	if err != nil {
		t.Fatalf("LPOP() error = %v", err)
	}
	if poppedValue != "value0" {
		t.Errorf("LPOP() = %v, want %v", poppedValue, "value0")
	}
	values, err = kv.LGET(key)
	if err != nil {
		t.Fatalf("LGET() error = %v", err)
	}
	expectedValues = expectedValues[1:]
	if len(values) != len(expectedValues) {
		t.Fatalf("LGET() = %v, want %v", values, expectedValues)
	}
	for i, v := range expectedValues {
		if values[i] != v {
			t.Errorf("LGET() = %v, want %v", values[i], v)
		}
	}
}

func TestKVS(t *testing.T) {
	key := "test_key1"
	initialValues := []string{"value1", "value2", "value3"}

	// Test SSET
	if err := kv.SSET(key, initialValues...); err != nil && err != sql.ErrNoRows {
		t.Fatalf("SSET() error = %v", err)
	}
	values, err := kv.LGET(key)
	if err != nil && err != sql.ErrNoRows {
		t.Fatalf("LGET() error = %v", err)
	}
	if len(values) != len(initialValues) {
		t.Fatalf("LGET() = %v, want %v", values, initialValues)
	}
	for i, v := range initialValues {
		if values[i] != v {
			t.Errorf("LGET() = %v, want %v", values[i], v)
		}
	}

	// Test SSET with additional values
	additionalValues := []string{"value4", "value5"}
	if err := kv.SSET(key, additionalValues...); err != nil {
		t.Fatalf("SSET() error = %v", err)
	}
	values, err = kv.LGET(key)
	if err != nil {
		t.Fatalf("LGET() error = %v", err)
	}
	expectedValues := append(initialValues, additionalValues...)
	if len(values) != len(expectedValues) {
		t.Fatalf("LGET() = %v, want %v", values, expectedValues)
	}
	for i, v := range expectedValues {
		if values[i] != v {
			t.Errorf("LGET() = %v, want %v", values[i], v)
		}
	}

	// Test SDEL
	if err := kv.SDEL(key, "value2", "value4"); err != nil {
		t.Fatalf("SDEL() error = %v", err)
	}
	values, err = kv.LGET(key)
	if err != nil {
		t.Fatalf("LGET() error = %v", err)
	}
	expectedValues = []string{"value1", "value3", "value5"}
	if len(values) != len(expectedValues) {
		t.Fatalf("LGET() = %v, want %v", values, expectedValues)
	}
	for i, v := range expectedValues {
		if values[i] != v {
			t.Errorf("LGET() = %v, want %v", values[i], v)
		}
	}
}

func TestKVV(t *testing.T) {
	key := "test_key3"
	updatedValue := "updated_value"
	kvs := map[string]string{
		"key1": "value1",
		"key2": "value2",
		"key3": "value3",
	}

	// Test SET
	if err := kv.SET(kvs); err != nil {
		t.Fatalf("SET() error = %v", err)
	}

	// Test GET
	for k, v := range kvs {
		got, err := kv.GET(k)
		if err != nil {
			t.Fatalf("GET() error = %v", err)
		}
		if got != v {
			t.Errorf("GET() = %v, want %v", got, v)
		}
	}

	// Test GETAll
	allKVs, err := kv.GETALL()
	if err != nil {
		t.Fatalf("GETAll() error = %v", err)
	}
	for k, v := range kvs {
		if allKVs[k] != v {
			t.Errorf("GETAll() = %v, want %v", allKVs[k], v)
		}
	}

	// Test GETALLSTRUCT()
	var results []*kvStruct
	if err := kv.GETALLSTRUCT("ASC", &results); err != nil {
		t.Fatalf("GETALLSTRUCT() error = %v", err)
	}

	// Test SET with updated value
	updatedKVs := map[string]string{
		key: updatedValue,
	}
	if err := kv.SET(updatedKVs); err != nil {
		t.Fatalf("SET() error = %v", err)
	}
	got, err := kv.GET(key)
	if err != nil {
		t.Fatalf("GET() error = %v", err)
	}
	if got != updatedValue {
		t.Errorf("GET() = %v, want %v", got, updatedValue)
	}

	// Test DEL
	if err := kv.DEL(key); err != nil {
		t.Fatalf("DEL() error = %v", err)
	}
	got, err = kv.GET(key)
	if err != nil && err != sql.ErrNoRows {
		t.Fatalf("GET() error = %v", err)
	}
	if got != "" {
		t.Errorf("GET() = %v, want %v", got, "")
	}
}
