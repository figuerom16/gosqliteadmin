package moxylib

import (
	"database/sql"
	"errors"
	"fmt"
	"maps"
	"slices"
	"strings"
)

const separator = "|"

type KVTable struct {
	Table string
	CK    string
	CV    string
}

// Create a KVTable struct
func KVNew(table, key, value string) *KVTable {
	return &KVTable{Table: table, CK: key, CV: value}
}

// DELETES ROW FROM TABLE. Only this function will delete the row from the table.
// All other functions will empty the value field.
func (t *KVTable) DEL(key string) error {
	_, err := DB.Exec(fmt.Sprintf("DELETE FROM %s WHERE %s = ?;", t.Table, t.CK), key)
	if err != nil {
		return err
	}
	return nil
}

// Get Value from a given table and key.
func (t *KVTable) GET(key string) (string, error) {
	row := DB.QueryRow(fmt.Sprintf("SELECT %s FROM %s WHERE %s = ?;", t.CV, t.Table, t.CK), key)
	value := ""
	if err := row.Scan(&value); err != nil {
		return "", err
	}
	return value, nil
}

// Retrieves all key-value pairs from a given table and returns them in a map. OPTIONS: ASC, DESC, or blank for no order.
func (t *KVTable) GETALL() (map[string]string, error) {
	rows, err := DB.Query(fmt.Sprintf("SELECT %s, %s FROM %s;", t.CK, t.CV, t.Table))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	kvs := make(map[string]string)
	for rows.Next() {
		var key, value string
		if err := rows.Scan(&key, &value); err != nil {
			return nil, err
		}
		kvs[key] = value
	}
	return kvs, nil
}

// Retrieves all key-value pairs from a given table and returns them in the provided STRUCT. OPTIONS: ASC, DESC, or blank for no order.
func (t *KVTable) GETALLSTRUCT(order string, v any) error {
	order = strings.ToUpper(order)
	if order != "" && order != "ASC" && order != "DESC" {
		return errors.New("order must be ASC or DESC or blank for no order")
	}
	if order != "" {
		order = " ORDER BY " + t.CK + " " + order
	}
	rows, err := DB.Query(fmt.Sprintf("SELECT %s, %s FROM %s %s;", t.CK, t.CV, t.Table, order))
	if err != nil {
		return err
	}
	if err := RowsScan(rows, v); err != nil {
		return err
	}
	return nil
}

// Updates the table with the given key-value pairs. If map is nil then do nothing.
func (t *KVTable) SET(kvs map[string]string) error {
	if kvs == nil {
		return nil
	}
	values := make([]any, 0, 2*len(kvs))
	for key, value := range kvs {
		values = append(values, key, value)
	}
	placeholders := strings.TrimSuffix(strings.Repeat("(?, ?),", len(kvs)), ",")
	query := fmt.Sprintf("INSERT OR REPLACE INTO %s (%s, %s) VALUES %s", t.Table, t.CK, t.CV, placeholders)
	if _, err := DB.Exec(query, values...); err != nil {
		return err
	}
	return nil
}

// Delete fields from a hash in a given table.
func (t *KVTable) HDEL(key string, fields ...string) error {
	values, err := t.GET(key)
	if err != nil {
		return err
	}
	kvs := SplitIntoMap(values, separator)
	for _, field := range fields {
		delete(kvs, field)
	}
	values = ""
	for field, value := range kvs {
		values += fmt.Sprintf("%s%s%s\n", field, separator, value)
	}
	if err := t.SET(map[string]string{key: values}); err != nil {
		return err
	}
	return nil
}

// Retrieves the values of fields from a hash in a given table. If no fields are specified, all fields are returned.
func (t *KVTable) HGET(key string, fields ...string) (map[string]string, error) {
	values, err := t.GET(key)
	if err != nil {
		return nil, err
	}
	kvs := SplitIntoMap(values, separator)
	if len(fields) == 0 {
		return kvs, nil
	}
	result := make(map[string]string)
	for _, field := range fields {
		if value, exists := kvs[field]; exists {
			result[field] = unescape(value)
		}
	}
	return result, nil
}

// Sets the values of fields in a hash in a given table.
func (t *KVTable) HSET(key string, kvs map[string]string) error {
	values, err := t.GET(key)
	if err != nil && err != sql.ErrNoRows {
		return err
	}
	currentkvs := SplitIntoMap(values, separator)
	for field, value := range kvs {
		if strings.Contains(field, separator) {
			return fmt.Errorf("field cannot contain separator %s", separator)
		}
		currentkvs[field] = escape(value)
	}
	values = ""
	for field, value := range currentkvs {
		values += fmt.Sprintf("%s%s%s\n", field, separator, value)
	}
	if err := t.SET(map[string]string{key: values}); err != nil {
		return err
	}
	return nil
}

// Checks if a field exists in a set/list in a given table.
func (t *KVTable) LEXISTS(key, value string) (bool, error) {
	values, err := t.LGET(key)
	if err != nil {
		return false, err
	}
	for _, v := range values {
		if v == value {
			return true, nil
		}
	}
	return false, nil
}

// Get list from a given table and key.
func (t *KVTable) LGET(key string) ([]string, error) {
	values, err := t.GET(key)
	if err != nil {
		return nil, err
	}
	if values == "" {
		return []string{}, nil
	}
	arrValues := strings.Split(values, "\n")
	for i, v := range arrValues {
		arrValues[i] = unescape(v)
	}
	return arrValues, nil
}

// Retrieve and remove the first or last element from a list in a given table.
func (t *KVTable) LPOP(key string, end bool) (string, error) {
	values, err := t.LGET(key)
	if err != nil {
		return "", err
	}
	if len(values) == 0 {
		return "", errors.New("empty list")
	}
	var field string
	if end {
		field, values = values[len(values)-1], values[:len(values)-1]
	} else {
		field, values = values[0], values[1:]
	}
	if err := t.SET(map[string]string{key: strings.Join(values, "\n")}); err != nil {
		return "", err
	}
	return unescape(field), nil
}

// Add a field to the beginning or end of a list in a given table.
func (t *KVTable) LPUSH(key, field string, end bool) error {
	values, err := t.LGET(key)
	if err != nil {
		return err
	}
	if end {
		values = append(values, escape(field))
	} else {
		values = append([]string{field}, values...)
	}
	if err := t.SET(map[string]string{key: strings.Join(values, "\n")}); err != nil {
		return err
	}
	return nil
}

// Deletes fields from a set in a given table.
func (t *KVTable) SDEL(key string, values ...string) error {
	currentValues, err := t.LGET(key)
	if err != nil {
		return err
	}
	valueMap := make(map[string]struct{}, len(currentValues))
	for _, v := range currentValues {
		valueMap[v] = struct{}{}
	}
	for _, v := range values {
		delete(valueMap, v)
	}
	if err := t.SET(map[string]string{key: strings.Join(slices.Sorted(maps.Keys(valueMap)), "\n")}); err != nil {
		return err
	}
	return nil
}

// Sets values in a set in a given table.
func (t *KVTable) SSET(key string, values ...string) error {
	if len(values) == 0 {
		return nil
	}
	currentValues, err := t.LGET(key)
	if err != nil {
		return err
	}
	for _, v := range values {
		currentValues = append(currentValues, escape(v))
	}
	valueMap := make(map[string]struct{}, len(currentValues))
	for _, v := range currentValues {
		valueMap[v] = struct{}{}
	}
	if err := t.SET(map[string]string{key: strings.Join(slices.Sorted(maps.Keys(valueMap)), "\n")}); err != nil {
		return err
	}
	return nil
}

// Helper function to split a string into a map of key-value pairs.
func SplitIntoMap(s, sep string) map[string]string {
	m := make(map[string]string)
	for _, line := range strings.Split(s, "\n") {
		if parts := strings.SplitN(line, sep, 2); len(parts) == 2 {
			m[parts[0]] = parts[1]
		}
	}
	return m
}

func escape(s string) string {
	strings.ReplaceAll(s, "\r", "")
	return strings.ReplaceAll(s, "\n", "\\n")
}

func unescape(s string) string {
	return strings.ReplaceAll(s, "\\n", "\n")
}
