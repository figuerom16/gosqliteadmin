package moxylib

import (
	"testing"
)

func TestCipherEncryptDecrypt(t *testing.T) {
	originalData := "This is a test string."
	encryptedData, err := CipherEncrypt([]byte(originalData))
	if err != nil {
		t.Fatalf("CipherEncrypt failed: %v", err)
	}
	decryptedData, err := CipherDecrypt([]byte(encryptedData))
	if err != nil {
		t.Fatalf("CipherDecrypt failed: %v", err)
	}
	if string(decryptedData) != originalData {
		t.Errorf("Decrypted data does not match original data. Got: %s, Want: %s", decryptedData, originalData)
	}
}

func TestPasswordHashAndVerify(t *testing.T) {
	password := "supersecretpassword"
	hash, err := PasswordHash(password)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	valid, _, err := PasswordVerify(password, hash)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if !valid {
		t.Error("expected password to be valid")
	}
	// Test with an invalid password
	valid, _, err = PasswordVerify("wrongpassword", hash)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if valid {
		t.Error("expected password to be invalid")
	}
}
