package moxylib

import (
	"context"
	"errors"
	"fmt"
	"net/smtp"
	"reflect"
	"strconv"
	"strings"
	"sync"

	"github.com/go-playground/form/v4"
	"github.com/go-playground/mold/v4/modifiers"
	"github.com/go-playground/validator/v10"
)

var (
	conform  = modifiers.New()
	ctx      = context.Background()
	decoder  = form.NewDecoder()
	validate = validator.New(validator.WithRequiredStructEnabled())
)

// GoRangeWait is a concurrent function that waits at the end.
// It will run the gofunc for each element in the list concurrently.
// This is suitable for mutexed operations in gofunc.
func GoRangeWait[E any](list []E, gofunc func(int, E) error) []error {
	errs := make([]error, len(list))
	noErr := true
	var wg sync.WaitGroup
	wg.Add(len(list))
	for i, l := range list {
		go func() {
			defer wg.Done()
			if err := gofunc(i, l); err != nil {
				errs[i] = err
				noErr = false
			}
		}()
	}
	wg.Wait()
	if noErr {
		return nil
	}
	return errs
}

// StructValues converts a struct to a slice of string values.
func StructMap(v any) map[string]string {
	val := reflect.ValueOf(v).Elem()
	if val.NumField() < 2 {
		return nil
	}
	return map[string]string{fmt.Sprintf("%v", val.Field(0).Interface()): fmt.Sprintf("%v", val.Field(1).Interface())}
}

// Post parser for form data.
func Transform(v any) error {
	if err := conform.Struct(ctx, v); err != nil {
		return err
	}
	if err := validate.Struct(v); err != nil {
		var errs []string
		for _, err := range err.(validator.ValidationErrors) {
			if strings.ToLower(err.Field()) == "password" {
				errs = append(errs, fmt.Sprintf("field password: must be %s %s characters", err.Tag(), err.Param()))
				continue
			}
			errs = append(errs, fmt.Sprintf("field %s: wanted %s %s, got `%s`", err.Field(), err.Tag(), err.Param(), err.Value()))
		}
		return errors.New(strings.Join(errs, "\n"))
	}
	return nil
}

// SendEmail sends an email using the provided recipient with HTML content.
func SendHTMLEmail(subject, html string, to ...string) error {
	if EmailAuth == nil {
		return errors.New("email authentication not set")
	}
	message := fmt.Sprintf("From: %s\nTo: %s\nSubject: %s\nMIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n%s", Settings["EmailUsername"], strings.Join(to, ","), subject, html)
	err := smtp.SendMail(fmt.Sprintf("%s:%s", Settings["EmailHost"], Settings["EmailPort"]), EmailAuth, Settings["EmailUsername"], to, []byte(message))
	if err != nil {
		return fmt.Errorf("failed to send email: %w", err)
	}
	return nil
}

// ValidateBool checks if a string is a boolean.
func ValidateBool(setting string) error {
	setting = strings.ToLower(setting)
	if setting != "true" && setting != "false" {
		return errors.New(setting + " must be true or false")
	}
	return nil
}

// ValidateInt checks if a string is an integer.
func ValidateInt(setting string) error {
	if _, err := strconv.Atoi(setting); err != nil {
		return errors.New(setting + " must be an integer")
	}
	return nil
}
