package moxylib

import (
	"testing"
)

func TestExecSQLite(t *testing.T) {
	result := ExecSQLite("CREATE TABLE test2 (id INTEGER PRIMARY KEY, name TEXT);")
	if len(result) != 3 || result[1][0] != "ROWS_AFFECTED" {
		t.Errorf("ExecSQLite() = %v, want [[RESULT INFO] [ROWS_AFFECTED 0] [LAST_INSERT_ID 0]]", result)
	}

	result = ExecSQLite("INSERT INTO test2 (name) VALUES ('Alice');")
	if len(result) != 3 || result[1][0] != "ROWS_AFFECTED" || result[2][0] != "LAST_INSERT_ID" {
		t.Errorf("ExecSQLite() = %v, want [[RESULT INFO] [ROWS_AFFECTED 1] [LAST_INSERT_ID 1]]", result)
	}
}

func TestQuerySQLite(t *testing.T) {
	ExecSQLite("CREATE TABLE test3 (id INTEGER PRIMARY KEY, name TEXT);")
	ExecSQLite("INSERT INTO test3 (name) VALUES ('Alice');")

	result := QuerySQLite("SELECT * FROM test3;", false)
	if len(result) != 2 || result[0][0] != "id[INTEGER]" || result[1][1] != "Alice" {
		t.Errorf("QuerySQLite() = %v, want [[id[INTEGER] name[TEXT]] [1 Alice]]", result)
	}

	result = QuerySQLite("SELECT * FROM test3;", true)
	if len(result) != 2 || result[0][0] != "id" || result[1][1] != "Alice" {
		t.Errorf("QuerySQLite() = %v, want [[id name] [1 Alice]]", result)
	}
}

func TestRunSQLite(t *testing.T) {
	sql := `
CREATE TABLE test4 (id INTEGER PRIMARY KEY, name TEXT);
INSERT INTO test4 (name) VALUES ('Alice');
SELECT * FROM test4;
`
	result := RunSQLite(sql)
	if len(result) != 3 {
		t.Fatalf("RunSQLite() = %v, want 3 results", result)
	}

	if len(result[0]) != 3 || result[0][1][0] != "ROWS_AFFECTED" {
		t.Errorf("RunSQLite() CREATE TABLE = %v, want [[RESULT INFO] [ROWS_AFFECTED 0] [LAST_INSERT_ID 0]]", result[0])
	}

	if len(result[1]) != 3 || result[1][1][0] != "ROWS_AFFECTED" || result[1][2][0] != "LAST_INSERT_ID" {
		t.Errorf("RunSQLite() INSERT INTO = %v, want [[RESULT INFO] [ROWS_AFFECTED 1] [LAST_INSERT_ID 1]]", result[1])
	}

	if len(result[2]) != 2 || result[2][0][0] != "id[INTEGER]" || result[2][1][1] != "Alice" {
		t.Errorf("RunSQLite() SELECT * = %v, want [[id[INTEGER] name[TEXT]] [1 Alice]]", result[2])
	}
}

func TestFilterSQL(t *testing.T) {
	sql := `
-- This is a comment
CREATE TABLE test5 (id INTEGER PRIMARY KEY, name TEXT);
INSERT INTO test5 (name) VALUES ('Alice');
SELECT * FROM test5;

`
	expected := []string{
		"CREATE TABLE test5 (id INTEGER PRIMARY KEY, name TEXT);",
		"INSERT INTO test5 (name) VALUES ('Alice');",
		"SELECT * FROM test5;",
	}
	result := FilterSQL(sql)
	if len(result) != len(expected) {
		t.Fatalf("FilterSQL() = %v, want %v", result, expected)
	}
	for i, r := range result {
		if r != expected[i] {
			t.Errorf("FilterSQL() = %v, want %v", result, expected)
		}
	}
}
