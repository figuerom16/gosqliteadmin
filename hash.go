package moxylib

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"log"

	"golang.org/x/sys/cpu"

	"github.com/alexedwards/argon2id"
	"github.com/klauspost/compress/zstd"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/chacha20poly1305"
)

var (
	aead cipher.AEAD
	zEnc *zstd.Encoder
	zDec *zstd.Decoder
)

func init() {
	key := make([]byte, chacha20poly1305.KeySize)
	rand.Read(key)
	var err error
	if cpu.X86.HasAES || cpu.ARM64.HasAES || cpu.ARM.HasAES {
		block, err := aes.NewCipher(key)
		if err != nil {
			Close(err)
		}
		aead, err = cipher.NewGCM(block)
		if err != nil {
			Close(err)
		}
	} else {
		aead, err = chacha20poly1305.NewX(key)
		if err != nil {
			Close(err)
		}
	}
	zEnc, err = zstd.NewWriter(nil)
	if err != nil {
		Close(err)
	}
	zDec, err = zstd.NewReader(nil, zstd.WithDecoderConcurrency(0))
	if err != nil {
		Close(err)
	}
	log.Println("Crypto Initialized")
}

// Basic Hash
func BasicHash(plain ...[]byte) string {
	hasher, _ := blake2b.New256(nil)
	for _, p := range plain {
		hasher.Write(p)
	}
	return base64.RawURLEncoding.EncodeToString(hasher.Sum(nil))
}

// Compresses and encrypts data.
func CipherEncrypt(plain []byte) (string, error) {
	if len(plain) == 0 {
		return "", errors.New("cipher: data is empty")
	}
	plain = zEnc.EncodeAll(plain, make([]byte, 0, len(plain)))
	nonce := make([]byte, aead.NonceSize())
	rand.Read(nonce)
	return base64.RawURLEncoding.EncodeToString(aead.Seal(nonce, nonce, plain, nil)), nil
}

// Decrypts and decompresses data.
func CipherDecrypt(encrypted []byte) ([]byte, error) {
	if len(encrypted) == 0 {
		return nil, errors.New("cipher: data is empty")
	}
	cipherBytes, err := base64.RawURLEncoding.DecodeString(string(encrypted))
	if err != nil {
		return nil, err
	}
	decrypted, err := aead.Open(nil, cipherBytes[:aead.NonceSize()], cipherBytes[aead.NonceSize():], nil)
	if err != nil {
		return nil, err
	}
	decrypted, err = zDec.DecodeAll(decrypted, nil)
	if err != nil {
		return nil, err
	}
	return decrypted, nil
}

// Hard to crack password hashing with Argon2id.
func PasswordHash(key string) (string, error) {
	if key == "" {
		return "", errors.New("passwordhash: key is empty")
	}
	return argon2id.CreateHash(key, &argon2id.Params{Memory: 64 * 1024, Iterations: 1, Parallelism: 1, SaltLength: 16, KeyLength: 32})
}

// PasswordVerify checks if a key matches a hash.
func PasswordVerify(key, hash string) (bool, *argon2id.Params, error) {
	if key == "" || hash == "" {
		return false, nil, nil
	}
	return argon2id.CheckHash(key, hash)
}

// RandomString generates a random string of a given length.
func RandomString(length int) string {
	bytes := make([]byte, length)
	if _, err := rand.Read(bytes); err != nil {
		return ""
	}
	return base64.RawURLEncoding.EncodeToString(bytes)
}
