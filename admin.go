package moxylib

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
)

// execSQLite executes a given SQL command and returns the results in a 2D string slice.
// The result slice contains information about the execution, such as errors, rows affected, and last insert ID.
func ExecSQLite(cmd string) [][]string {
	result := [][]string{{"RESULT", "INFO"}}
	res, err := DB.Exec(cmd)
	if err != nil {
		return append(result, []string{"ERROR", err.Error()})
	}
	rowsAffected, err := res.RowsAffected()
	if err == nil {
		result = append(result, []string{"ROWS_AFFECTED", strconv.FormatInt(rowsAffected, 10)})
	}
	lastInsertId, err := res.LastInsertId()
	if err == nil {
		result = append(result, []string{"LAST_INSERT_ID", strconv.FormatInt(lastInsertId, 10)})
	}
	return result
}

// Remove leading/trailing whitespace/comments and split the SQL string into individual commands.
func FilterSQL(sql string) []string {
	var filtered []string
	var command strings.Builder
	for _, line := range strings.Split(sql, "\n") {
		line = strings.TrimSpace(line)
		if line == "" || strings.HasPrefix(line, "--") {
			continue
		}
		if command.Len() > 0 {
			command.WriteString("\n")
		}
		command.WriteString(line)
		if strings.HasSuffix(line, ";") {
			filtered = append(filtered, command.String())
			command.Reset()
		}
	}
	if command.Len() > 0 {
		filtered = append(filtered, command.String())
	}
	return filtered
}

// Executes a given SQL query and returns the results in a 2D string slice.
// The result slice contains the query results, including column headers and data rows.
// If noCalc is true, the column headers will not include the database type.
func QuerySQLite(cmd string, noCalc bool) [][]string {
	var result [][]string
	rows, err := DB.Query(cmd)
	if err != nil {
		return append(result, []string{"", ""}, []string{"ERROR", err.Error()})
	}
	defer rows.Close()
	types, err := rows.ColumnTypes()
	if err != nil {
		return append(result, []string{"", ""}, []string{"ERROR", err.Error()})
	}
	var header []string
	for _, t := range types {
		if noCalc {
			header = append(header, t.Name())
			continue
		}
		dbType := t.DatabaseTypeName()
		if dbType == "" {
			dbType = "CALC"
		}
		header = append(header, fmt.Sprintf("%s[%s]", t.Name(), dbType))
	}
	result = append(result, header)
	values := make([]sql.RawBytes, len(header))
	pointers := make([]any, len(values))
	for i := range values {
		pointers[i] = &values[i]
	}
	for rows.Next() {
		if err := rows.Scan(pointers...); err != nil {
			return append(result, []string{"", ""}, []string{"ERROR", err.Error()})
		}
		valueStrings := make([]string, len(values))
		for i, v := range values {
			valueStrings[i] = string(v)
		}
		result = append(result, valueStrings)
	}
	return result
}

// Take any string and attempt to execute it as a SQLite command.
// Returns a 3D array of strings with the result of the command.
func RunSQLite(sql string) [][][]string {
	var result [][][]string
	for _, q := range FilterSQL(sql) {
		if q == "" {
			continue
		}
		lower := strings.ToLower(q)
		selec := strings.HasPrefix(lower, "select")
		with := strings.HasPrefix(lower, "with")
		pragma := strings.HasPrefix(lower, "pragma")
		explain := strings.HasPrefix(lower, "explain")
		if selec || with || pragma || explain {
			result = append(result, QuerySQLite(q, pragma || explain))
		} else {
			result = append(result, ExecSQLite(q))
		}
	}
	return result
}
