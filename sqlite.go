package moxylib

import (
	"bufio"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/smtp"
	"os"
	"reflect"
	"strconv"
	"strings"

	_ "github.com/ncruces/go-sqlite3/driver"
	_ "github.com/ncruces/go-sqlite3/embed"
)

type MDB struct{ *sql.DB }

// URIOptimizations for other SQLite3 drivers.
const (
	URIOptimizations = "?_pragma=busy_timeout(10000)&_pragma=journal_mode(WAL)&_pragma=journal_size_limit(200000000)&_pragma=synchronous(NORMAL)&_pragma=foreign_keys(ON)&_pragma=temp_store(MEMORY)&_pragma=cache_size(-16000)"
	BackupPath       = "sqlite/backup"
)

var (
	DB        *MDB                               // DB is the global database connection.
	EmailAuth smtp.Auth                          // EmailAuth is the SMTP authentication.
	Settings  = make(map[string]string)          // Settings is a map of key-value pairs from the setting table.
	KVSetting = KVNew("setting", "Key", "Value") // KVSetting is the key-value setting table.
)

// Connects to the SQLite3 database and sets the global DB variable.
// nil defaults to ncruces SQLite3 driver.
// Also initializes the crypto variables and loads the settings table.
func Setup(newDB *sql.DB) error {
	if err := os.MkdirAll(BackupPath, 0770); err != nil && !os.IsExist(err) {
		return err
	}
	if newDB == nil {
		var err error
		if newDB, err = sql.Open("sqlite3", "file:sqlite/db.sqlite3"+URIOptimizations); err != nil {
			return err
		}
	}
	DB = &MDB{newDB}
	log.Println("Database Connected")
	if err := Migrate("migrations.sql"); err != nil {
		return err
	}
	if len(os.Args) > 3 && os.Args[1] == "moxylib" {
		confmap := make(map[string]string)
		for i := 2; i < len(os.Args)-1; i += 2 {
			key, value := os.Args[i], os.Args[i+1]
			if key == "AdminSecret" {
				var err error
				value, err = PasswordHash(value)
				if err != nil {
					return err
				}
			}
			confmap[key] = value
		}
		if err := KVSetting.SET(confmap); err != nil {
			return err
		}
		return nil
	}
	if err := UpdateSettings(nil); err != nil {
		return err
	}
	return nil
}

// Closes the database connection and logs any errors.
func Close(err error) {
	if err != nil {
		DB.Close()
		log.Fatalln(err)
	}
	if err := DB.Close(); err != nil {
		log.Println(err)
	}
	log.Println("Database Closed")
}

// Retrieves the user_version PRAGMA from the database and returns it as an integer.
// The user_version is used to track the current version of the database schema.
func GetUserVersion() (int, error) {
	userVersion := 0
	if err := DB.QueryRow("PRAGMA user_version;").Scan(&userVersion); err != nil {
		return 0, err
	}
	return userVersion, nil
}

// Uses ReadMigrate to read the commands from the SQL file and applies them to the database.
func Migrate(path string) error {
	migrations, err := ReadMigrations(path)
	if err != nil {
		return err
	}
	if len(migrations) == 0 {
		return nil
	}
	userVersion, err := GetUserVersion()
	if err != nil {
		return err
	}
	for userVersion < len(migrations) {
		if err := Transaction(FilterSQL(migrations[userVersion])...); err != nil {
			return fmt.Errorf("error: %v\nkey: %d\nmigration: %s", err, userVersion, migrations[userVersion])
		}
		userVersion++
		if _, err := DB.Exec(fmt.Sprintf("PRAGMA user_version = %d;", userVersion)); err != nil {
			return err
		}
		log.Println("Database Migrated", userVersion)
	}
	return nil
}

// Executes a series of SQL commands within a single transaction.
func Transaction(execs ...string) error {
	tx, err := DB.Begin()
	if err != nil {
		return err
	}
	for _, exec := range execs {
		if _, err := tx.Exec(exec); err != nil {
			tx.Rollback()
			return err
		}
	}
	if err := tx.Commit(); err != nil {
		return err
	}
	return nil
}

// Updates the settings table with the given key-value pairs.
// Update Settings map with the new values.
func UpdateSettings(confmap map[string]string) error {
	var err error
	if confmap != nil {
		for key, value := range confmap {
			if strings.Contains(key, "Secret") && value == "" {
				delete(confmap, key)
			}
		}
		if secret, ok := confmap["AdminSecret"]; ok {
			if confmap["AdminSecret"], err = PasswordHash(secret); err != nil {
				return err
			}
		}
	}
	if err := KVSetting.SET(confmap); err != nil {
		return err
	}
	Settings, err = KVSetting.GETALL()
	if err != nil {
		return err
	}
	log.Println("Settings Loaded")
	if Settings["EmailUsername"] != "" && Settings["EmailToken"] != "" && Settings["EmailHost"] != "" && Settings["EmailPort"] != "" {
		EmailAuth = smtp.PlainAuth("", Settings["EmailUsername"], Settings["EmailToken"], Settings["EmailHost"])
		log.Println("Email Auth Set")
	}
	if Settings["LimiterBurst"] != "" && Settings["LimiterRate"] != "" && Settings["LimiterBanCount"] != "" {
		LimiterBurst, err = strconv.Atoi(Settings["LimiterBurst"])
		if err != nil {
			LimiterBurst = 100
		}
		LimiterRate, err = strconv.Atoi(Settings["LimiterRate"])
		if err != nil {
			LimiterRate = 10
		}
		LimiterBanCount, err = strconv.Atoi(Settings["LimiterBanCount"])
		if err != nil {
			LimiterBanCount = 1000
		}
		if err := limitersLoad(); err != nil {
			log.Println("Limiters Not Loaded:", err)
		}
	}
	return nil
}

// Reads a SQL file containing multiple migration commands and returns a map of versioned SQL strings.
func ReadMigrations(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	migrations := []string{}
	migration := ""
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			continue
		}
		if strings.HasPrefix(line, "--moxylib") {
			if migration != "" {
				migrations = append(migrations, migration)
				migration = ""
				continue
			}
		}
		migration += line + "\n"
	}
	migrations = append(migrations, migration)
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return migrations, nil
}

// Takes SQL rows and scans them into a slice of structs.
func RowsScan(rows *sql.Rows, dest any) error {
	defer rows.Close()
	destVal := reflect.ValueOf(dest)
	if destVal.Kind() != reflect.Ptr || destVal.Elem().Kind() != reflect.Slice {
		return errors.New("destination must be a pointer to a slice")
	}
	sliceVal := destVal.Elem()
	elemType := sliceVal.Type().Elem()
	if elemType.Kind() != reflect.Ptr || elemType.Elem().Kind() != reflect.Struct {
		return errors.New("slice elements must be pointers to structs")
	}
	structType := elemType.Elem()
	columns, err := rows.Columns()
	if err != nil {
		return err
	}
	fieldMap := make(map[string]int)
	for i := 0; i < structType.NumField(); i++ {
		fieldMap[structType.Field(i).Name] = i
	}
	for rows.Next() {
		item := reflect.New(structType).Elem()
		pointers := make([]any, len(columns))
		for i, col := range columns {
			fieldIndex, ok := fieldMap[col]
			if !ok {
				return fmt.Errorf("no matching field found for column %s", col)
			}
			pointers[i] = item.Field(fieldIndex).Addr().Interface()
		}
		if err := rows.Scan(pointers...); err != nil {
			return err
		}
		sliceVal.Set(reflect.Append(sliceVal, item.Addr()))
	}
	reflect.ValueOf(dest).Elem().Set(sliceVal)
	return nil
}
